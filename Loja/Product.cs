﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja
{
    class Product
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SaleUnit { get; set; }
        public string PurchaseUnit { get; set; }
        public string Factor { get; set; }
        public int Amount { get; set; }
        public decimal CostPrice { get; set; }
        public decimal SellingPrice { get; set; }
        public int InitialInventory { get; set; }
        public decimal DaysOnInventory { get; set; }
        public DateTime? OlderInventoryItem { get; set; }
        public DateTime? LastSale { get; set; }
        public int AmountConsideredInCalculations { get; set; }
    }
}
