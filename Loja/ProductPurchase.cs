﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja
{
    class ProductPurchase : IComparable
    {
        public Product Product { get; set; }
        public DateTime Date { get; set; }
        public int Amount { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }

        public int CompareTo(object obj)
        {
            ProductPurchase otherProductPurchase = obj as ProductPurchase;
            return this.Date.CompareTo(otherProductPurchase.Date);
        }
    }
}
