﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja
{
    class ProductSale : IComparable
    {
        public Product Product { get; set; }
        public DateTime Date { get; set; }
        public int Amount { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }

        public int CompareTo(object obj)
        {
            ProductSale otherProductSale = obj as ProductSale;
            return this.Date.CompareTo(otherProductSale.Date);
        }
    }
}
