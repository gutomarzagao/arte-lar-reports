﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Collections;

namespace Loja
{
    class Program
    {
        private const string InventoryFilePath = @"C:\Users\v-gumarz\Desktop\loja\estoque.xlsx";
        private const string PurchasesFilePath = @"C:\Users\v-gumarz\Desktop\loja\compras.xlsx";
        private const string SalesFilePath = @"C:\Users\v-gumarz\Desktop\loja\vendas.xlsx";
        private const string ResultFilePath = @"C:\Users\v-gumarz\Desktop\loja\resultado.csv";

        private static Dictionary<string, Product> inventory = new Dictionary<string, Product>();
        private static LinkedList<ProductPurchase> purchases = new LinkedList<ProductPurchase>();
        private static LinkedList<ProductSale> sales = new LinkedList<ProductSale>();

        static void Main(string[] args)
        {
            ReadDataFromSpreadsheets();

            purchases = new LinkedList<ProductPurchase>(purchases.OrderBy(p => p.Date).AsEnumerable());
            sales = new LinkedList<ProductSale>(sales.OrderBy(s => s.Date).AsEnumerable());
            
            foreach (ProductSale sale in sales)
            {
                while (sale.Amount > 0)
                {
                    int initialInventory = sale.Product.InitialInventory;
                    if (initialInventory > 0)
                    {
                        int amountToBeDiscarted = Math.Min(sale.Amount, initialInventory);
                        sale.Product.InitialInventory -= amountToBeDiscarted;
                        sale.Amount -= amountToBeDiscarted;
                    }
                    else
                    {
                        LinkedListNode<ProductPurchase> purchaseNode = purchases.First;
                        ProductPurchase purchase = purchaseNode.Value;
                        while (purchase.Product != sale.Product)
                        {
                            purchaseNode = purchaseNode.Next;
                            purchase = purchaseNode.Value;
                        }

                        int amountToBeConsidered = Math.Min(sale.Amount, purchase.Amount);
                        int totalPeriod = (sale.Date - purchase.Date).Days;

                        sale.Product.DaysOnInventory += totalPeriod * amountToBeConsidered;
                        sale.Product.AmountConsideredInCalculations += amountToBeConsidered;
                        sale.Product.LastSale = sale.Date;

                        sale.Amount -= amountToBeConsidered;
                        purchase.Amount -= amountToBeConsidered;
                        if (purchase.Amount == 0)
                            purchases.Remove(purchaseNode);
                    }
                }
            }

            foreach (ProductPurchase purchase in purchases)
                if (purchase.Product.OlderInventoryItem == null)
                    purchase.Product.OlderInventoryItem = purchase.Date;

            WriteResultOnSpreadsheet();
        }

        private static void ReadDataFromSpreadsheets()
        {
            ReadInventoryData();
            ReadPurchaseData();
            ReadSaleData();
        }

        private static void ReadInventoryData()
        {
            Application excel = new Application();
            Workbook workbook = excel.Workbooks.Open(InventoryFilePath);
            Worksheet sheet = (Worksheet) workbook.Sheets["Sheet1"];
            object[,] inventorySheet = sheet.get_Range("A2:AS4573").Value2;

            int codeColumn = 1;
            int nameColumn = 5;
            int descriptionColumn = 6;
            int saleUnitColumn = 7;
            int purchaseUnitColumn = 8;
            int amountColumn = 18;
            int costPriceColumn = 22;
            int sellingPriceColumn = 23;

            for (int rowNumber = 1; rowNumber <= inventorySheet.GetLength(0); rowNumber++)
            {
                string code = inventorySheet[rowNumber, codeColumn].ToString();
                if (String.IsNullOrEmpty(code))
                    throw new InvalidOperationException("Código de produto vazio ou nulo.");

                if (inventory.ContainsKey(code))
                    throw new InvalidOperationException("Código de produto duplicado: " + code);

                Product product = new Product();

                product.Code = code;
                product.Name = inventorySheet[rowNumber, nameColumn].ToString().Trim();
                product.Description = inventorySheet[rowNumber, descriptionColumn].ToString().Trim();
                product.SaleUnit = inventorySheet[rowNumber, saleUnitColumn].ToString().Trim();
                product.PurchaseUnit = inventorySheet[rowNumber, purchaseUnitColumn].ToString().Trim();
                product.Amount = (int)Decimal.Parse(inventorySheet[rowNumber, amountColumn].ToString(), new CultureInfo("en-US"));
                product.CostPrice = Decimal.Parse(inventorySheet[rowNumber, costPriceColumn].ToString(), new CultureInfo("en-US"));
                product.SellingPrice = Decimal.Parse(inventorySheet[rowNumber, sellingPriceColumn].ToString(), new CultureInfo("en-US"));
                product.InitialInventory = product.Amount;

                if (product.SaleUnit == "UN" && product.PurchaseUnit == "UN")
                    inventory.Add(code, product);
            }

            workbook.Close();
        }

        private static void ReadPurchaseData()
        {
            Application excel = new Application();
            Workbook workbook = excel.Workbooks.Open(PurchasesFilePath);
            Worksheet sheet = (Worksheet)workbook.Sheets["Sheet1"];
            object[,] purchaseSheet = sheet.get_Range("A2:BH3503").Value2;

            int codeColumn = 5;
            int amountColumn = 10;
            int dateTimeColumn = 2;
            int totalPriceColumn = 15;

            for (int rowNumber = 1; rowNumber <= purchaseSheet.GetLength(0); rowNumber++)
            {
                string code = purchaseSheet[rowNumber, codeColumn].ToString();
                if (String.IsNullOrEmpty(code))
                    throw new InvalidOperationException("Código de produto vazio ou nulo.");

                if (!inventory.ContainsKey(code))
                    continue;

                ProductPurchase productPurchase = new ProductPurchase();

                productPurchase.Product = inventory[code];
                productPurchase.Amount = (int)Decimal.Parse(purchaseSheet[rowNumber, amountColumn].ToString(), new CultureInfo("en-US"));
                productPurchase.Date = DateTime.ParseExact(purchaseSheet[rowNumber, dateTimeColumn].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                productPurchase.TotalPrice = Decimal.Parse(purchaseSheet[rowNumber, totalPriceColumn].ToString(), new CultureInfo("en-US"));
                productPurchase.UnitPrice = productPurchase.TotalPrice / productPurchase.Amount;
                productPurchase.Product.InitialInventory -= productPurchase.Amount;

                purchases.AddLast(productPurchase);
            }

            workbook.Close();
        }

        private static void ReadSaleData()
        {
            Application excel = new Application();
            Workbook workbook = excel.Workbooks.Open(SalesFilePath);
            Worksheet sheet = (Worksheet)workbook.Sheets["Sheet1"];
            object[,] saleSheet = sheet.get_Range("A2:U10625").Value2;

            int codeColumn = 5;
            int amountColumn = 11;
            int dateTimeColumn = 17;
            int totalPriceColumn = 15;

            for (int rowNumber = 1; rowNumber <= saleSheet.GetLength(0); rowNumber++)
            {
                string code = saleSheet[rowNumber, codeColumn].ToString();
                if (String.IsNullOrEmpty(code))
                    throw new InvalidOperationException("Código de produto vazio ou nulo.");

                if (!inventory.ContainsKey(code))
                    continue;

                ProductSale productSale = new ProductSale();

                productSale.Product = inventory[code];
                productSale.Amount = (int)Decimal.Parse(saleSheet[rowNumber, amountColumn].ToString(), new CultureInfo("en-US"));
                productSale.Date = DateTime.ParseExact(saleSheet[rowNumber, dateTimeColumn].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                productSale.TotalPrice = Decimal.Parse(saleSheet[rowNumber, totalPriceColumn].ToString(), new CultureInfo("en-US"));
                productSale.UnitPrice = productSale.TotalPrice / productSale.Amount;
                productSale.Product.InitialInventory += productSale.Amount;

                sales.AddLast(productSale);
            }

            workbook.Close();
        }

        private static void WriteResultOnSpreadsheet()
        {
            string[] results = GetResultMatrix();
            File.WriteAllLines(ResultFilePath, results);
        }

        private static string[] GetResultMatrix()
        {
            string[] results = new string[inventory.Count + 2];
            string[] line = new string[8];

            results[0] = "sep=;";

            line[0] = "Codigo";
            line[1] = "Nome";
            line[2] = "Descricao";
            line[3] = "Qtd vendida";
            line[4] = "Permanencia media";
            line[5] = "Qtd em estoque";
            line[6] = "Em estoque desde";
            line[7] = "Ultima venda";
            results[1] = String.Join(";", line);

            int index = 2;
            foreach (Product product in inventory.Values)
            {
                line[0] = product.Code;
                line[1] = product.Name.Replace(",", "").Replace(";", "");
                line[2] = product.Description.Replace(",", "").Replace(";", "");
                line[3] = product.AmountConsideredInCalculations.ToString();

                if (product.AmountConsideredInCalculations > 0)
                    line[4] = (product.DaysOnInventory / product.AmountConsideredInCalculations).ToString();
                else
                    line[4] = "N/A";

                line[5] = product.Amount.ToString();

                if (product.OlderInventoryItem != null)
                    line[6] = String.Format("{0:dd/MM/yyyy}", product.OlderInventoryItem);
                else
                    line[6] = "N/A";

                if (product.LastSale != null)
                    line[7] = String.Format("{0:dd/MM/yyyy}", product.LastSale);
                else
                    line[7] = "N/A";

                results[index++] = String.Join(";", line);
            }

            return results;
        }
    }
}
